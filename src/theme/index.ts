import { createMuiTheme } from '@material-ui/core/styles';

import { colors, shadows } from './config';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: colors.primary.darker,
      main: colors.primary.dark2,
      dark: colors.primary.dark,
    },
    secondary: {
      light: colors.primary.divider,
      main: colors.primary.dark2,
      dark: colors.primary.dark,
    },
    error: {
      main: colors.primary.red,
    },
    grey: {
      50: '#FFFFFF',
      100: '#FAFAFA',
      200: '#F4F5F5',
      300: '#EEEEEF',
      400: '#E3E4E6',
      500: '#D5D6D9',
      600: '#C7C9CD',
      700: '#ACAEB3',
      800: '#747881',
      900: '#333333',
    },
    text: {
      // primary: colors.primary.text,
      primary: '#454545',
      //secondary: colors.primary.textNegative,
      secondary: '#454545',
    },
    background: {
      paper: colors.grey[50],
      default: colors.primary.dark,
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 450,
      md: 570,
      lg: 1050,
      xl: 1220
    },
  },
  spacing: 5,
  shape: {
    borderRadius: 4,
  },
  shadows: [
    'none',
    shadows[1],
    shadows[2],
    shadows[3],
    shadows[4],
    shadows[5],
    shadows[6],
    shadows[7],
    shadows[8],
    shadows[9],
    shadows[10],
    shadows[11],
    shadows[12],
    shadows[13],
    shadows[14],
    shadows[15],
    shadows[16],
    shadows[17],
    shadows[18],
    shadows[19],
    shadows[20],
    shadows[21],
    shadows[22],
    shadows[23],
    shadows[24],
  ],
  typography: {
    fontFamily: "'Source Sans Pro', sans-serif",
    fontSize: 12,
    h1: {
      fontFamily: 'Preto Semi',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '32px',
      lineHeight: '110%',
      // color: colors.primary.textNegative,
      color: '#454545',
    },
    h2: {
      fontFamily: 'Preto Semi',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '32px',
      lineHeight: '110%',
      // color: colors.primary.textNegative,
      color: '#454545',
    },
    h3: {
      fontFamily: 'Preto Semi',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '32px',
      lineHeight: '110%',
      // color: colors.primary.textNegative,
      color: '#454545',
    },
    h4: {
      fontFamily: 'Preto Semi',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '22px',
      lineHeight: '125%',
      // color: colors.primary.textNegative,
      color: '#454545',
    },
    h5: {
      fontFamily: 'Preto Semi',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '16px',
      lineHeight: '133%',
      // color: colors.primary.textDark,
      color: '#454545',
    },
    subtitle1: {
      fontFamily: 'Source Sans Pro',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '14px',
      lineHeight: '18px',
      color: colors.primary.text,
    },
    subtitle2: {
      fontFamily: 'Preto Semi',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '18px',
      lineHeight: '125%',
      color: colors.primary.textNegative,
    },
    body1: {
      fontFamily: 'Source Sans Pro',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '16px',
      lineHeight: '150%',
      color: colors.primary.text,
    },
    body2: {
      fontFamily: 'Source Sans Pro',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '14px',
      lineHeight: '18px',
      color: colors.primary.darker,
    },
    caption: {
      fontFamily: 'Source Sans Pro',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '16px',
      lineHeight: '20px',
      color: colors.primary.darker,
    },
  },
  overrides: {
    MuiInputLabel: {
      outlined: {
        transform: 'translate(14px, 10px) scale(1)',
      },
    },
    MuiButton: {
      containedPrimary: {
        backgroundColor: '#2c1c14',
        color: '#FFFFFF',
        fontSize: 14,
        fontWeight: 'normal',
        padding: '0 12.5px',
        boxShadow: 'none',
        height: 32,
        '&:hover': {
          backgroundColor: colors.primary.text,
          boxShadow: 'none',
        },
      },
      containedSecondary: {
        backgroundColor: colors.grey[50],
        color: colors.primary.textDark,
        fontSize: 14,
        fontWeight: 'normal',
        padding: '0 12.5px',
        boxShadow: 'none',
        height: 40,
        width: '100%',
        '&:hover': {
          backgroundColor: colors.primary.text,
          boxShadow: 'none',
        },
      },
      textSecondary: {
        color: colors.primary.text,
        '&:hover': {
          backgroundColor: 'transparent',
          textDecoration: 'underline',
        },
      },
      label: {
       textTransform: 'none',
      },
    },
    MuiDivider: {
      root: {
        backgroundColor: colors.primary.divider,
      },
    },
    MuiListItem: {
      root: {
        '&.Mui-selected, &.Mui-selected:hover, &:hover': {
          backgroundColor: 'transparent !important',
        },
      },
    },
    MuiListItemIcon: {
      root: {
        marginRight: 8,
        minWidth: 0,
      },
    },
    MuiCard: {
      root: {
        // backgroundColor: colors.primary.dark2,
        backgroundColor: colors.grey[50],
        boxShadow: 'none',
      },
    },
  },
});

export default theme;
