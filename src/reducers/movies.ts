export const INITIAL_STATE = {
  searchString: '',
  loading: false,
  Error: false,
  Response: false,
  Search: [],
  totalResults: 0,
  movieDetail: null,
  page: 1,
  newPageLoaded: false,
};

const movies = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case 'FETCH_MOVIES':
      return { 
        ...state,
        loading: true,
        Search: [],
        searchString: action.string || state.searchString,
        page: action.page || 1,
        newPageLoaded: false,
      };
    case 'MOVIE_RECEIVED':
      return {
        ...state,
        Error: false,
        ...action.payload,
        loading: false,
        newPageLoaded: true,
      };
    case 'FETCH_MOVIE_BY_ID':
      return { ...state, loading: true, movieDetail: null };
    case 'MOVIE_BY_ID_RECEIVED':
      return { ...state, loading: false, movieDetail: {...action.payload} };
    default:
      return state;
  }
};

export default movies;
