import { SET_ACTIVE_FILTERS } from '../actions/filters';

export const INITIAL_STATE = {
  // interest_area: [],
  // interest_point: [],
  // craft: [],
  // material: [],
};

const activeFilters = (state = INITIAL_STATE, action: any) => {
  const { type, params } = action;

  switch (type) {
    case SET_ACTIVE_FILTERS:
      return {
        ...params,
      };
    default:
      return state;
  }
};

export default activeFilters;
