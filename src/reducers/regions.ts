import { handleActions } from 'redux-actions';
import { handle } from 'redux-pack';

import { GET_REGIONS } from '../actions/regions';

export const INITIAL_STATE: any = {
  regions: [],
  regionsLoaded: false,
  error: false,
};

export default handleActions(
  {
    [GET_REGIONS](state: any, action: any) {
      return handle(state, action, {
        start: (prevState: any) => {
          return {
            ...prevState,
          };
        },
        finish: (prevState: any) => {
          return {
            ...prevState,
            regionsLoaded: true,
          };
        },
        failure: (prevState: any) => {
          return {
            ...prevState,
            error: action.payload,
          };
        },
        success: (prevState: any) => {
          return {
            ...prevState,
            regions: action.payload,
          };
        },
      });
    },
  },
  INITIAL_STATE,
);
