import { TOGGLE_MOBILE_FILTERS_OPEN } from '../actions/filters';

export const INITIAL_STATE = false;

const mobileFiltersOpen = (state = INITIAL_STATE, action: any) => {
  const { type, bool } = action;

  switch (type) {
    case TOGGLE_MOBILE_FILTERS_OPEN:
      return bool;
    default:
      return state;
  }
};

export default mobileFiltersOpen;
