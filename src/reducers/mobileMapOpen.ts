import { TOGGLE_MOBILE_MAP_OPEN } from '../actions/filters';

export const INITIAL_STATE = false;

const mobileMapOpen = (state = INITIAL_STATE, action: any) => {
  const { type, bool } = action;

  switch (type) {
    case TOGGLE_MOBILE_MAP_OPEN:
      return bool;
    default:
      return state;
  }
};

export default mobileMapOpen;
