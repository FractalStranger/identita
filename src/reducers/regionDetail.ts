import { handleActions } from 'redux-actions';
import { handle } from 'redux-pack';

import { GET_REGION_DETAIL, REMOVE_REGION_DETAIL } from '../actions/regions';
import { SET_ACTIVE_FILTERS } from '../actions/filters';

import PointShape from '../shapes/PointShape';
import FilterItemShape from '../shapes/FilterItemShape';

export const INITIAL_STATE: any = {
  regionDetail: {},
  regionDetailLoaded: false,
  regionDetailLoadedFirst: false,
  error: false,
  activePoints: [],
};

export default handleActions(
  {
    [GET_REGION_DETAIL](state: any, action: any) {
      return handle(state, action, {
        start: (prevState: any) => {
          return {
            ...prevState,
            regionDetailLoaded: false,
          };
        },
        finish: (prevState: any) => {
          return {
            ...prevState,
          };
        },
        failure: (prevState: any) => {
          return {
            ...prevState,
            error: action.payload,
          };
        },
        success: (prevState: any) => {
          return {
            ...prevState,
            regionDetail: action.payload,
            regionDetailLoaded: true,
            regionDetailLoadedFirst: true
          };
        },
      });
    },
    [REMOVE_REGION_DETAIL](state: any, action: any) {
      return INITIAL_STATE;
    },
    [SET_ACTIVE_FILTERS](state: any, action: any) {
      return {
        ...state,
        activePoints: state.regionDetail.points && state.regionDetail.points.filter((item: PointShape) => {
          let isActive = 1;
          for (let index = 0; index < Object.keys(action.params).length; index++) {
            isActive = item[Object.keys(action.params)[index]].filter((filterItem: FilterItemShape) =>
              action.params[Object.keys(action.params)[index]].indexOf(filterItem.id) !== -1).length;
            if (isActive === 0) break;
          }
          return isActive;
        }),
      };
    },
  },
  INITIAL_STATE,
);
