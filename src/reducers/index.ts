import { combineReducers } from 'redux';

import regions from './regions';
import regionDetail from './regionDetail';
import activeFilters from './activeFilters';
import mobileFiltersOpen from './mobileFiltersOpen';
import mobileMapOpen from './mobileMapOpen';

export default combineReducers({
  regions,
  regionDetail,
  activeFilters,
  mobileFiltersOpen,
  mobileMapOpen
});