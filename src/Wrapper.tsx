import React, { useEffect, useState } from 'react';
import { useLocation, useParams, useHistory } from 'react-router-dom';

import { CircularProgress, Box, Hidden } from '@material-ui/core';

import RegionShape from './shapes/RegionShape';
import RegionDetailShape from './shapes/RegionDetailShape';
import ActiveFiltersShape from './shapes/ActiveFiltersShape';

import Filters from './containers/FiltersContainer';
import MainBar from './containers/MainBarContainer';
import MobileMenuBar from './containers/MobileMenuBarContainer';

type Props = {
  getRegions: () => void;
  regionsLoaded: boolean;
  error: any;
  regions: RegionShape[];
  getRegionDetail: (region: RegionDetailShape) => void;
  regionDetailLoaded: boolean;
  regionDetailLoadedFirst: boolean;
  regionDetailError: any;
  setActiveFilters: (params: ActiveFiltersShape) => void;
  removeRegionDetail: () => void;
};

function Wrapper({
  getRegions,
  regionsLoaded,
  error,
  regions,
  getRegionDetail,
  regionDetailLoaded,
  regionDetailLoadedFirst,
  regionDetailError,
  setActiveFilters,
  removeRegionDetail,
}: Props) {
  const location = useLocation();
  const params = useParams();
  const history = useHistory();
  const [regionNotFound, setRegionNotFound] = useState(false);

  useEffect(() => {
    if (error) return;
    if (!regionsLoaded) {
      getRegions();
      return;
    }
    const { region } = params;
    if (region === 'slovensko') {
      // TODO - remove data from redux
      removeRegionDetail();
      return;
    }
    if (regions.find((item: RegionShape) => item.slug === region)) {
      getRegionDetail(region);
    }
    else {
      setRegionNotFound(true);
    }
  }, [params.region, regionsLoaded]);
  
  useEffect(() => {
    if (!regionDetailLoaded) return;
    const searchParams = new URLSearchParams(location.search);
    const paramsArray = Array.from(searchParams.entries());
    let queryParams: any = {};

    for (var i = 0; i < paramsArray.length; ++i)
      queryParams = {
        ...queryParams,
        [paramsArray[i][0]]: paramsArray[i][1].split(','),
      };
    setActiveFilters(queryParams);
  }, [location.search, regionDetailLoaded]);

  if (regionNotFound) return (
    <div>Region not found</div>
  );

  return (
    <>
      <Box display="flex" flex="auto" className="background-dark">
        {regionsLoaded ? (
          // TODO -  USE THIS
          <>
            <Filters
              history={history}
              region={params.region}
              regionDetailLoaded={regionDetailLoaded}
            />

            {!error ? (
              <MainBar
                region={params.region}
                regionDetailLoadedFirst={regionDetailLoadedFirst}
              />
            ) : (
              <Box
                height="100%"
                width="100%"
                display="flex"
                alignItems="center"
                justifyContent="center"
                position="absolute"
                left="0"
                top="0"
                bgcolor="rgba(0, 0, 0, 0.5)"
                color="#FFF"
              >
                {error.message}
              </Box>
            )}
            <Hidden lgUp implementation="js">
              <MobileMenuBar />
            </Hidden>
          </>
        ) : (
          <>
            {params.region !== "slovensko" && (
              <Box
                height="100%"
                width="100%"
                display="flex"
                alignItems="center"
                justifyContent="center"
                position="absolute"
                left="0"
                top="0"
                bgcolor="rgba(0, 0, 0, 0.5)"
                color="#FFF"
              >
                {error ? (
                  error.message
                ) : regionDetailError ? (
                  regionDetailError.message
                ) : (
                  <CircularProgress />
                )}
              </Box>
            )}
          </>
        )}
      </Box>
    </>
  );  
}

export default Wrapper;
