import RegionDetailShape from '../shapes/RegionDetailShape';

export const GET_REGIONS = 'GET_REGIONS';
export const GET_REGION_DETAIL = 'GET_REGION_DETAIL';
export const REMOVE_REGION_DETAIL = 'REMOVE_REGION_DETAIL';

export function getRegions() {
  const resultsResource = fetch(`${process.env.REACT_APP_ENDPOINT}regions?_format=json`);
  const success = (res: any) => res.json();

  return {
    type: GET_REGIONS,
    promise: resultsResource.then(success),
  };
}

export function getRegionDetail(regionSlug: RegionDetailShape) {
  const resultsResource = fetch(`${process.env.REACT_APP_ENDPOINT}map/${regionSlug}?_format=json`);
  const success = (res: any) => res.json();

  return {
    type: GET_REGION_DETAIL,
    promise: resultsResource.then(success),
  };
}

export function removeRegionDetail() {
  return {
    type: REMOVE_REGION_DETAIL,
  };
}