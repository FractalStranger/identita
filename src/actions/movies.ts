// export const FETCH_MOVIES = 'FETCH_MOVIES';

export const fetchMovies = (string: string, page: number) => ({
  type: 'FETCH_MOVIES',
  string,
  page,
});

export const fetchMovieById = (id: string) => ({
  type: 'FETCH_MOVIE_BY_ID',
  id,
});
