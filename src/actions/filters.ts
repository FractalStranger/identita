import ActiveFiltersShape from '../shapes/ActiveFiltersShape';

export const SET_ACTIVE_FILTERS = 'SET_ACTIVE_FILTERS';
export const TOGGLE_MOBILE_FILTERS_OPEN = 'TOGGLE_MOBILE_FILTERS_OPEN';
export const TOGGLE_MOBILE_MAP_OPEN = 'TOGGLE_MOBILE_MAP_OPEN';

export function setActiveFilters(params: ActiveFiltersShape) {
  return {
    type: SET_ACTIVE_FILTERS,
    params,
  };
}

export function toggleMobileFiltersOpen(bool: boolean) {
  return {
    type: TOGGLE_MOBILE_FILTERS_OPEN,
    bool,
  };
}

export function toggleMobileMapOpen(bool: boolean) {
  return {
    type: TOGGLE_MOBILE_MAP_OPEN,
    bool,
  };
}
