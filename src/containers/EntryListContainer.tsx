import { connect } from "react-redux";

import EntryList from "../components/MainBar/EntryList";

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    activePoints: state.regionDetail.activePoints,
  };
}

export default connect(mapStateToProps, {
  // fetch,
})(EntryList);
