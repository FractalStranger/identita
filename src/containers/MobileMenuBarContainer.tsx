import { connect } from 'react-redux';

import MobileMenuBar from '../components/MobileMenuBar';

import { toggleMobileFiltersOpen, toggleMobileMapOpen } from '../actions/filters';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    mobileFiltersOpen: state.mobileFiltersOpen,
    mobileMapOpen: state.mobileMapOpen,
  };
}

export default connect(
  mapStateToProps,
  {
    toggleMobileFiltersOpen,
    toggleMobileMapOpen,
  },
)(MobileMenuBar);
