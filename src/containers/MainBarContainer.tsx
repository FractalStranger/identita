import { connect } from 'react-redux';

import MainBar from '../components/MainBar';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    regionDetail: state.regionDetail.regionDetail,
    regionDetailLoaded: state.regionDetail.regionDetailLoaded,
    mobileMapOpen: state.mobileMapOpen,
    activePoints: state.regionDetail.activePoints,
  };
}

export default connect(
  mapStateToProps,
  {
    // fetch,
  },
)(MainBar);
