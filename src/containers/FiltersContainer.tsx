import { connect } from 'react-redux';

import Filters from '../components/Filters';

import { toggleMobileFiltersOpen } from '../actions/filters';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    regions: state.regions.regions,
    activeFilters: state.activeFilters,
    activePoints: state.regionDetail.activePoints,
    allPoints: state.regionDetail.regionDetail.points,
    mobileFiltersOpen: state.mobileFiltersOpen,
    regionDetailLoadedFirst: state.regionDetail.regionDetailLoadedFirst,
    regionDetailError: state.regionDetail.error,
  };
}

export default connect(
  mapStateToProps,
  {
    toggleMobileFiltersOpen,
  },
)(Filters);
