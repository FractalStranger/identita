import { connect } from 'react-redux';

import Wrapper from '../Wrapper';

import { getRegions, getRegionDetail, removeRegionDetail } from '../actions/regions';
import { setActiveFilters } from '../actions/filters';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    ...state.regions,
    regionDetailLoaded: state.regionDetail.regionDetailLoaded,
    regionDetailLoadedFirst: state.regionDetail.regionDetailLoadedFirst,
    regionDetailError: state.regionDetail.error,
    allPoints: state.regionDetail.regionDetail.points,
  };
}

export default connect(
  mapStateToProps,
  {
    getRegions,
    getRegionDetail,
    setActiveFilters,
    removeRegionDetail,
  },
)(Wrapper);
