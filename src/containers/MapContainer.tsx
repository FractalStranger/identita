import { connect } from 'react-redux';

import Map from '../components/Map';

import { toggleMobileMapOpen } from '../actions/filters';

function mapStateToProps(state: any, ownProps: any) {
  return {
    ...ownProps,
    regionParams: state.regionDetail.regionDetail.params,
    activePoints: state.regionDetail.activePoints,
    mobileMapOpen: state.mobileMapOpen,
    regionDetailLoadedFirst: state.regionDetail.regionDetailLoadedFirst,
    regionDetailError: state.regionDetail.error,
  };
}

export default connect(
  mapStateToProps,
  {
    toggleMobileMapOpen,
  },
)(Map);
