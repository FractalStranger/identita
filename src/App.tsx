import React from 'react';
import { Route } from 'react-router-dom';

import Wrapper from './containers/WrapperContainer';

function App() {
  return (
    <div className="App">
      <Route path="/mapa/:region" component={Wrapper} />
    </div>
  );
}

export default App;
