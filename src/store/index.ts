import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { middleware as reduxPackMiddleware } from 'redux-pack';

// import logger from 'redux-logger';

import reducer from '../reducers';

// function saveToLocalStorage(state: any) {
//   try {
//     let newState = {...state};
//     newState = {
//       favourite: newState.favourite,
//     };
//     const serializedState = JSON.stringify(newState);
//     localStorage.setItem('state', serializedState);
//   } catch (e) {
//     // statements
//     console.log(e);
//   }
// }

// function loadFromLocalStorage() {
//   try {
//     const serializedState = localStorage.getItem('state');
//     if (serializedState === null) return undefined;
//     return JSON.parse(serializedState);
//   } catch (e) {
//     // statements
//     console.log(e);
//     return undefined;
//   }
// }

// const persistedState = loadFromLocalStorage();

let middleware = <any>[];
if (process.env.NODE_ENV === "development") {
  middleware = [
    ...middleware,
    thunk,
    reduxPackMiddleware,
    require("redux-logger").default,
  ];
} else {
  middleware = [...middleware, thunk, reduxPackMiddleware];
}

const store = createStore(
  reducer,
  // persistedState,
  applyMiddleware(...middleware)
);

// store.subscribe(() => saveToLocalStorage(store.getState()));

export default store;
