import React from 'react';

import { withStyles, WithStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';

interface Props {
  height?: number;
}

function RadioButton({ classes, height }: WithStyles & Props) {
  let divStyle = {};
  if (height) {
    divStyle = { height: `${height}px` };
  }
  return (
    <div className={classes.root} style={divStyle}>
      <SvgIcon className={classes.icon} viewBox="0 0 16 16">
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect x="0.5" y="0.5" width="15" height="15" rx="7.5" stroke="#D2C0A8" />
        </svg>
      </SvgIcon>
    </div>
  );
}

export default withStyles((theme) => ({
  root: {},
  icon: {
    height: '100%',
    width: 'auto',
  },
}))(RadioButton);
