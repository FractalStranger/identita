import React from "react";

import { withStyles, WithStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";

interface Props {
  height?: number;
}

function Pin({ classes, height }: WithStyles & Props) {
  let divStyle = {};
  if (height) {
    divStyle = { height: `${height}px`, display: "flex" };
  }
  return (
    <div className={classes.root} style={divStyle}>
      <SvgIcon className={classes.icon} viewBox="0 0 20 20">
        <svg
          width="20"
          height="20"
          viewBox="0 0 20 20"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M4.1665 8.33333C4.1665 5.10833 6.77484 2.5 9.99984 2.5C13.2248 2.5 15.8332 5.10833 15.8332 8.33333C15.8332 11.5583 9.99984 17.5 9.99984 17.5C9.99984 17.5 4.1665 11.5583 4.1665 8.33333ZM9.99984 4.16667C7.69984 4.16667 5.83317 6.03333 5.83317 8.33333C5.83317 9.76667 8.02484 12.7917 9.99984 15.05C11.9748 12.7917 14.1665 9.76667 14.1665 8.33333C14.1665 6.03333 12.2998 4.16667 9.99984 4.16667ZM11.6665 8.33333C11.6665 9.25381 10.9203 10 9.99984 10C9.07936 10 8.33317 9.25381 8.33317 8.33333C8.33317 7.41286 9.07936 6.66667 9.99984 6.66667C10.9203 6.66667 11.6665 7.41286 11.6665 8.33333Z"
            fill="#D2C0A8"
          />
        </svg>
      </SvgIcon>
    </div>
  );
}

export default withStyles(theme => ({
  root: {},
  icon: {
    height: "100%",
    width: "auto"
  }
}))(Pin);
