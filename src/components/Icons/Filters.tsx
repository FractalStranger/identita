import React from "react";

import { withStyles, WithStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";

interface Props {
  height?: number;
}

function Filters({ classes, height }: WithStyles & Props) {
  let divStyle = {};
  if (height) {
    divStyle = { height: `${height}px`, display: "flex" };
  }
  return (
    <div className={classes.root} style={divStyle}>
      <SvgIcon className={classes.icon} viewBox="0 0 19 18">
        <svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M6.6422 10.8486V17.252C6.73771 18.1925 7.65983 18.0566 7.84352 17.8509L11.0617 15.4225C11.2491 15.2792 11.3593 15.0588 11.3593 14.8237V10.8486L17.8729 1.16826C18.2697 0.529023 17.7002 0 17.252 0H0.749468C-0.0220238 0.0257164 -0.135911 0.863336 0.128601 1.16826L6.6422 10.8486ZM4.16016 2.4989L9.01615 10.2021L13.8413 2.4989H4.16016Z"
            fill="#2F2F2F"
          />
        </svg>
      </SvgIcon>
    </div>
  );
}

export default withStyles(theme => ({
  root: {},
  icon: {
    height: "100%",
    width: "auto"
  }
}))(Filters);
