import React from 'react';

import { withStyles, WithStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';

interface Props {
  height?: number;
}

function RadioChecked({ classes, height }: WithStyles & Props) {
  let divStyle = {};
  if (height) {
    divStyle = { height: `${height}px` };
  }
  return (
    <div className={classes.root} style={divStyle}>
      <SvgIcon className={classes.icon} viewBox="0 0 16 16">
        <svg width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="16" height="16" rx="8" fill="#D2C0A8" />
          <g filter="url(#filter0_d)">
            <rect x="4" y="4" width="8" height="8" rx="4" fill="white" />
          </g>
          <defs>
            <filter id="filter0_d" x="0" y="2" width="16" height="16" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
              <feFlood floodOpacity="0" result="BackgroundImageFix" />
              <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
              <feOffset dy="2" />
              <feGaussianBlur stdDeviation="2" />
              <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0" />
              <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
              <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
            </filter>
          </defs>
        </svg>
      </SvgIcon>
    </div>
  );
}

export default withStyles((theme) => ({
  root: {},
  icon: {
    height: '100%',
    width: 'auto',
  },
}))(RadioChecked);
