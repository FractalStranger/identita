import React from "react";

import { withStyles, WithStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";

interface Props {
  height?: number;
}

function PinDark({ classes, height }: WithStyles & Props) {
  let divStyle = {};
  if (height) {
    divStyle = { height: `${height}px`, display: "flex" };
  }
  return (
    <div className={classes.root} style={divStyle}>
      <SvgIcon className={classes.icon} viewBox="0 0 14 18">
        <svg width="14" height="18" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M0 7C0 3.13 3.13 0 7 0C10.87 0 14 3.13 14 7C14 10.87 7 18 7 18C7 18 0 10.87 0 7ZM7 2C4.24 2 2 4.24 2 7C2 8.72 4.63 12.35 7 15.06C9.37 12.35 12 8.72 12 7C12 4.24 9.76 2 7 2ZM9 7C9 8.10457 8.10457 9 7 9C5.89543 9 5 8.10457 5 7C5 5.89543 5.89543 5 7 5C8.10457 5 9 5.89543 9 7Z"
            fill="#2F2F2F"
          />
        </svg>
      </SvgIcon>
    </div>
  );
}

export default withStyles(theme => ({
  root: {},
  icon: {
    height: "100%",
    width: "auto"
  }
}))(PinDark);
