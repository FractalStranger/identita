import React from 'react';

import { withStyles, WithStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';

interface Props {
  height?: number;
}

function Checkbox({ classes, height }: WithStyles & Props) {
  let divStyle = {};
  if (height) {
    divStyle = { height: `${height}px`, display: 'flex' };
  }
  return (
    <div className={classes.root} style={divStyle}>
      <SvgIcon className={classes.icon} viewBox="0 0 17 16">
        <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect x="0.785645" y="0.5" width="15" height="15" rx="1.5" stroke="#D2C0A8" />
        </svg>
      </SvgIcon>
    </div>
  );
}

export default withStyles((theme) => ({
  root: {},
  icon: {
    height: '100%',
    width: 'auto',
  },
}))(Checkbox);
