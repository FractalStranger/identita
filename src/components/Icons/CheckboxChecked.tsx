import React from 'react';

import { withStyles, WithStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';

interface Props {
  height?: number;
}

function CheckboxChecked({ classes, height }: WithStyles & Props) {
  let divStyle = {};
  if (height) {
    divStyle = { height: `${height}px`, display: 'flex' };
  }
  return (
    <div className={classes.root} style={divStyle}>
      <SvgIcon className={classes.icon} viewBox="0 0 17 16">
        <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect x="0.285645" width="16" height="16" rx="2" fill="#D2C0A8" />
          <path
            d="M7.44582 11.7799C7.32582 11.9133 7.15249 11.9933 6.96582 11.9999C6.78582 12.0066 6.60582 11.9333 6.47915 11.8066L4.28582 9.60661C4.02582 9.34661 4.02582 8.92661 4.28582 8.66661C4.54582 8.40661 4.96582 8.40661 5.22582 8.66661L6.79915 10.2399C6.83249 10.2733 6.87249 10.2866 6.91915 10.2866C6.96582 10.2866 7.00582 10.2666 7.03915 10.2333L12.0058 4.71327C12.2525 4.43994 12.6725 4.41994 12.9458 4.66661C13.2192 4.91327 13.2392 5.33327 12.9925 5.60661L7.44582 11.7799Z"
            fill="white"
          />
        </svg>
      </SvgIcon>
    </div>
  );
}

export default withStyles((theme) => ({
  root: {},
  icon: {
    height: '100%',
    width: 'auto',
  },
}))(CheckboxChecked);
