import React, { useState, useEffect, useCallback } from 'react';
import lodash from 'lodash';
import classnames from 'classnames';

import {
  withStyles,
  WithStyles,
  Button,
  Box,
  Popover,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
  Checkbox,
  Hidden,
  Typography,
} from '@material-ui/core';

import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import RadioDefaultIcon from '../Icons/RadioDefault';
import RadioCheckedIcon from '../Icons/RadioChecked';
import CheckboxIcon from '../Icons/Checkbox';
import CheckboxCheckedIcon from '../Icons/CheckboxChecked';

import FilterTypeShape from '../../shapes/FilterTypeShape';
import ActiveFiltersShape from '../../shapes/ActiveFiltersShape';
import RegionShape from '../../shapes/RegionShape';
import PointShape from '../../shapes/PointShape';
import FilterItemShape from '../../shapes/FilterItemShape';

import filterItems from '../../constants/filterItems';

type Props = {
  data: FilterTypeShape;
  region: string;
  history: any;
  regions: RegionShape[] | null;
  activeFilters: ActiveFiltersShape;
  activePoints: PointShape[];
  allPoints: PointShape[];
  toggleMobileFiltersOpen: (hiddenNavigation: boolean) => void;
  regionDetailLoadedFirst: boolean;
  regionDetailError: any;
};

function FilterItem({
  classes,
  data,
  region,
  history,
  regions,
  activeFilters,
  activePoints,
  allPoints,
  toggleMobileFiltersOpen,
  regionDetailLoadedFirst,
  regionDetailError,
}: Props & WithStyles) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [chosenRegion, setChosenRegion] = useState(region);
  const [chosenRegionName, setChosenRegionName] = useState('');
  const [checked, setChecked] = useState<any[]>([]);
  const [filters, setFilters] = useState<any[]>([]);
  const [filteredPointsNumber, setFilteredPointsNumber] = useState(0);
  const open = Boolean(anchorEl);
  let usedFilters: any[] = [];

  const filterIsActive = useCallback((point: any, name: string) => {
    return point[name].filter((item: any) => activeFilters[name].indexOf(item.id) !== -1).length > 0;
  }, [activeFilters]);

  const removeIncompatibleFilters = useCallback(() => {
    let filteredActiveFilters: ActiveFiltersShape = {
      ...activeFilters,
    };
    const typeIndex = filterItems.findIndex((el: FilterTypeShape) => el.type === data.type);

    const intersectionPoints = allPoints.filter((point: PointShape) => {
      return point[data.type].filter((pointFilter: FilterItemShape) => checked.indexOf(pointFilter.id) !== -1).length;
    });

    for (let index = 4; index > typeIndex; index--) {
      if (activeFilters[filterItems[index].type]) {
        activeFilters[filterItems[index].type].forEach((filterId: string) => {
          const totalIntersectionPoints = intersectionPoints.filter((poi: PointShape) => {
            return poi[filterItems[index].type].filter((poiFilter: FilterItemShape) => poiFilter.id === filterId).length;
          });
          // delete item(filter.id) from object when there are no intersetion points
          if (!totalIntersectionPoints.length) {
            const indexOfItem = filteredActiveFilters[filterItems[index].type].indexOf(filterId);
            if (index > -1) {
              filteredActiveFilters[filterItems[index].type].splice(indexOfItem, 1);
            }
          }
        });
      }
    }
    return filteredActiveFilters;
  }, [allPoints, activeFilters, checked, data.type]);

  const getFilteredPointsNumber = useCallback((checked: any[]) => {
    let futureActiveFilters = removeIncompatibleFilters();
    if (checked.length) {
      futureActiveFilters = {
        ...futureActiveFilters,
        [data.type]: checked,
      };
    } else {
      delete futureActiveFilters[data.type];
    }
    if (lodash.isEmpty(futureActiveFilters)) return allPoints.length;

    const points = allPoints.filter((point: PointShape) => {
      for (let index = 1; index < filterItems.length; index++) {
        if (futureActiveFilters[filterItems[index].type]) {
          return futureActiveFilters[filterItems[index].type].filter((filterId: string) => {
            return point[filterItems[index].type].filter((poiFilter: FilterItemShape) => poiFilter.id === filterId).length;
          }).length;
        }
      }
    });

    return points.length;
  }, [removeIncompatibleFilters, data.type, allPoints]);

  const handleSubmit = useCallback(() => {
    toggleMobileFiltersOpen(false);
    setAnchorEl(null);
    if (data.type === 'region') {
      history.push(`/mapa/${chosenRegion}`);
      return;
    }
    let queryParams: { [key: string]: any } = {};
    let filteredActiveFilters = removeIncompatibleFilters();
    Object.keys(filteredActiveFilters).forEach((key: string) => {
      if (filteredActiveFilters[key].length) {
        queryParams = {
          ...queryParams,
          [key]: filteredActiveFilters[key].join(','),
        };
      }
    });
    if (checked.length) {
      queryParams = {
        ...queryParams,
        [data.type]: checked.join(','),
      };
    }
    else {
      delete queryParams[data.type];
    }
    history.push({
      pathname: `/mapa/${chosenRegion}`,
      search: "?" + new URLSearchParams(queryParams).toString(),
    });
  }, [checked, chosenRegion, data.type, history, removeIncompatibleFilters, toggleMobileFiltersOpen]);

  // const getFilterItems = useCallback(() => {
  //   if (data.type === 'region') return;
  //   usedFilters = [];
  //   setFilters([]);
    
  //   let points = activePoints;
  //   if (lastFilter === data.type) points = allPoints; // TODO - this is wrong

  //   points.map((point: PointShape) => {
  //       point[data.type].map((filter: FilterItemShape) => {
  //         if (usedFilters.filter((f: FilterItemShape) => (f.id === filter.id)).length) return;
  //         usedFilters.push(filter);
  //         setFilters(usedFilters);
  //       });
  //   });
  // }, [activePoints, data.type, setFilters, usedFilters, allPoints]);

  const getFilterItems = useCallback(() => {
    if (data.type === 'region') return;
    usedFilters = [];
    setFilters([]);
    allPoints.forEach((point: PointShape) => {
      point[data.type].forEach((filter: FilterItemShape) => {
        if (usedFilters.filter((f: FilterItemShape) => (f.id === filter.id)).length) return;
        if (!lodash.isEmpty(activeFilters)) {
          let isActive = true;
          const typeIndex = filterItems.findIndex((el: FilterTypeShape) => el.type === data.type);
          // check previous filters if there is intersection
          for (let index = 1; index < typeIndex; index++) {
            if (activeFilters[filterItems[index].type]) {
              if (!filterIsActive(point, filterItems[index].type)) {
                isActive = false;
              }
            }
          }
          if (isActive) {
            usedFilters.push(filter);
          }
        } else {
          usedFilters.push(filter);
        }
        setFilters(usedFilters);
      });
    });
  }, [data.type, setFilters, usedFilters, allPoints, activeFilters, filterItems]);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = useCallback(() => {
    // TODO - reset filter
    if (data.type !== 'region') {
      if (activeFilters && activeFilters[data.type]) {
        setChecked(activeFilters[data.type]);
      } else {
        setChecked([]);
      }
    } else {
      setChosenRegion(region);
    }
    setAnchorEl(null);
  }, [activeFilters, data.type, region]);

  const handleChangeRegion = useCallback((reg: string) => {
    setChosenRegion(reg);
  }, []);

  const handleToggle = useCallback((value: string) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);

    setFilteredPointsNumber(getFilteredPointsNumber(newChecked));
  }, [checked, getFilteredPointsNumber]);

  useEffect(() => {
    setChosenRegion(region);
    if (region === "slovensko" || !regionDetailLoadedFirst || regionDetailError) {
      setFilters([]);
      setChecked([]);
      return;
    }
    getFilterItems();
    if (activeFilters && data.type !== 'region' && activeFilters[data.type]) {
      setChecked(activeFilters[data.type]);
    } else {
      setChecked([]);
    }
  }, [region, activeFilters, data.type]);

  useEffect(() => {
    if (activePoints) {
      setFilteredPointsNumber(activePoints.length);
      return;
    }
    setFilteredPointsNumber(0);
  }, [activePoints]);

  useEffect(() => {
    if (regions) {
      let regionName;
      const theRegion = regions.find((regionItem: RegionShape) => regionItem.slug === chosenRegion);
      if (!theRegion) {
        regionName = data.label;
      } else {
        regionName = theRegion.title;
      }

      setChosenRegionName(regionName);
    }
  }, [region]);

  if (!(filters.length || data.type === "region")) return null;

  return (
    <>
      <Box className={classes.wrapper}>
        <Button
          // aria-describedby={id}
          type="button"
          onClick={
            filters.length || data.type === "region"
              ? handleClick
              : undefined
          }
          className={classnames(data.type === "region" && classes.regionButton)}
          classes={{
            root: classes.dropdownButton,
            disabled: classes.disabledButton,
            label: classes.dropdownButtonLabel,
          }}
          disabled={!(filters.length || data.type === "region")}
        >
          {data.type === "region" ? (
            <>
              <span className={classes.desktopLabel}>
                {chosenRegionName}
              </span>
              <span className={classes.mobileLabel}>
                {data.label}
              </span>
            </>
          ) : (
            <>
              {data.label}
              {activeFilters[data.type] && activeFilters[data.type].length && (
                <span className={classes.filterCounter}>
                  {`(${activeFilters[data.type].length})`}
                </span>
              )}
            </>
          )}
          <ChevronRightIcon className={classes.chevron} />
        </Button>
        {(!!filters.length || data.type === "region") && (
          <Hidden lgUp implementation="css">
            <Divider />
          </Hidden>
        )}
      </Box>
      {data.type !== "region" && !filters.length ? null : (
        <Popover
          // id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "left",
          }}
          classes={{
            root: classes.popover,
            paper: classes.popoverPaper,
          }}
        >
          <Hidden lgUp implementation="css">
            <Box className={classes.mobileBar}>
              <Button variant="text" onClick={handleClose}>
                <Typography
                  component="span"
                  variant="subtitle1"
                  className={classes.bold}
                >
                  <ChevronLeftIcon /> Späť
                </Typography>
              </Button>
              <Divider className={classes.divider2} />
            </Box>
          </Hidden>
          <Hidden lgUp implementation="css">
            <Box pl={4} pr={4} mt={5} mb={1}>
              <Typography variant="h5" className={classes.label}>
                {data.label}
              </Typography>
            </Box>
          </Hidden>
          <List classes={{ root: classes.listRoot }}>
            {data.type === "region" && regions ? (
              <>
                <ListItem
                  button
                  selected={chosenRegion === 'slovensko'}
                  onClick={(event) => handleChangeRegion('slovensko')}
                  classes={{ root: classes.listItem }}
                >
                  <ListItemIcon>
                    {chosenRegion === 'slovensko' ? (
                      <RadioCheckedIcon height={16} />
                    ) : (
                      <RadioDefaultIcon height={16} />
                    )}
                  </ListItemIcon>
                  <ListItemText primary="Slovensko" />
                </ListItem>
                {regions.map((regionItem: RegionShape) => (
                  <ListItem
                    key={regionItem.slug}
                    button
                    selected={chosenRegion === regionItem.slug}
                    onClick={(event) => handleChangeRegion(regionItem.slug)}
                    classes={{ root: classes.listItem }}
                  >
                    <ListItemIcon>
                      {chosenRegion === regionItem.slug ? (
                        <RadioCheckedIcon height={16} />
                      ) : (
                        <RadioDefaultIcon height={16} />
                      )}
                    </ListItemIcon>
                    <ListItemText primary={regionItem.title} />
                  </ListItem>
                ))}
              </>
            ) : (
              <>
                {filters.map((filter: FilterItemShape) => (
                  <ListItem
                    key={filter.id}
                    button
                    onClick={handleToggle(filter.id)}
                    classes={{ root: classes.listItem }}
                  >
                    <ListItemIcon>
                      <Checkbox
                        checked={checked.indexOf(filter.id) !== -1}
                        tabIndex={-1}
                        icon={<CheckboxIcon height={16} />}
                        checkedIcon={<CheckboxCheckedIcon height={16} />}
                        disableRipple
                        // inputProps={{ 'aria-labelledby': labelId }}
                      />
                    </ListItemIcon>
                    <ListItemText primary={filter.label} />
                  </ListItem>
                ))}
              </>
            )}
          </List>
          <Box className={classes.footer}>
            <Hidden mdDown implementation="css">
              <Divider className={classes.divider} />
              <Button
                variant="text"
                color="secondary"
                classes={{ label: classes.cancelButtonLabel }}
                onClick={handleClose}
              >
                Zrušiť
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={handleSubmit}
              >
                Potvrdiť
              </Button>
            </Hidden>
            <Hidden lgUp implementation="css">
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSubmit}
              >
                {data.type !== "region"
                  ? `Zobraziť ${filteredPointsNumber} výsledkov`
                  : "Zobraziť"}
              </Button>
            </Hidden>
          </Box>
        </Popover>
      )}
    </>
  );
}

export default withStyles((theme) => ({
  wrapper: {
    display: 'inline-block',
    float: 'left',
    margin: `0 ${theme.spacing(1.5)}px`,
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  dropdownButton: {
    backgroundColor: theme.palette.grey[50],
    borderRadius: theme.shape.borderRadius,
    height: 40,
    padding: `0 ${theme.spacing(3)}px`,
    position: 'relative',
    border: '1px solid #e5dbcf',
    '&:hover': {
      backgroundColor: theme.palette.grey[50],
    },
    [theme.breakpoints.down('md')]: {
      width: '100%',
      backgroundColor: theme.palette.primary.main,
      borderRadius: 0,
      height: 56,
      color: theme.palette.grey[50],
      border: 'none',
      '&:hover': {
        color: theme.palette.text.primary,
      },
    },
  },
  disabledButton: {
    backgroundColor: theme.palette.grey[500],
    display: 'none',
    // [theme.breakpoints.down('md')]: {
    //   display: 'none',
    // },
  },
  dropdownButtonLabel: {
    fontSize: '16px',
    fontWeight: 'normal',
    lineHeight: '17.6px',
    justifyContent: 'flex-start',
    paddingRight: theme.spacing(11),
  },
  chevron: {
    position: 'absolute',
    right: theme.spacing(2),
    transform: 'rotate(90deg)',
    fontSize: '24px',
    [theme.breakpoints.down('md')]: {
      transform: 'rotate(0deg)',
      color: theme.palette.primary.light,
    },
  },
  popoverPaper: {
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[1],
    minWidth: 320,
    marginTop: theme.spacing(1),
    [theme.breakpoints.down('md')]: {
      backgroundColor: theme.palette.primary.dark,
      width: '100%',
      height: 'calc(100% - 16px)',
      position: 'fixed',
      left: '0 !important',
      top: '0 !important',
      margin: 0,
      right: '0 !important',
      maxWidth: 'none',
      maxHeight: 'calc(100% - 16px)',
      boxShadow: 'none',
      borderRadius: 0,
      overflowY: 'hidden',
    },
  },
  listRoot: {
    overflowY: 'auto',
    height: 'auto',
    maxHeight: '300px',
    [theme.breakpoints.down('md')]: {
      height: 'calc(100% - 158px)',
      maxHeight: 'none',
    },
    '@media (max-height: 400px)': {
      maxHeight: 'calc(100vh - 100px)',
    },
  },
  listItem: {
    paddingTop: 4,
    paddingBottom: 4,
    [theme.breakpoints.down('md')]: {
      '& .MuiTypography-root, & svg': {
        color: theme.palette.primary.light,
      },
    },
  },
  listText: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '14px',
    lineHeight: '18px',
  },
  divider: {
    position: 'absolute',
    left: 0,
    width: '100%',
    top: 0,
    opacity: 0.2,
  },
  footer: {
    backgroundColor: theme.palette.grey[50],
    display: 'flex',
    justifyContent: 'space-between',
    height: 58,
    alignItems: 'flex-end',
    padding: theme.spacing(2.5),
    position: 'sticky',
    bottom: 0,
    '& > div:first-child': {
      [theme.breakpoints.up('lg')]: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        width: '100%',
      },
    },
    [theme.breakpoints.down('md')]: {
      backgroundColor: theme.palette.primary.dark,
      position: 'fixed',
      bottom: 0,
      left: 0,
      justifyContent: 'center',
      width: '100%',
      paddingLeft: theme.spacing(4),
      paddingRight: theme.spacing(4),
      '& > div': {
        width: '100%',
      },
    },
  },
  cancelButtonLabel: {
    fontSize: '14px',
    lineHeight: '17.6px',
    textAlign: 'left',
    justifyContent: 'flex-start',
  },
  mobileBar: {
    height: 56,
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
  },
  bold: {
    fontWeight: 'bold',
    color: theme.palette.primary.light,
    display: 'flex',
    alignItems: 'center',
  },
  divider2: {
    [theme.breakpoints.down('md')]: {
      position: 'absolute',
      left: 0,
      top: 56,
      width: '100%',
    },
  },
  regionButton: {
    justifyContent: 'flex-start',
    [theme.breakpoints.up('lg')]: {
      width: '140px',
    },
    '& .MuiButton-label': {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      textAlign: 'left',
      paddingRight: 0,
      width: '80px',
      display: 'flex',
    },
  },
  desktopLabel: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  mobileLabel: {
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
  filterCounter: {
    position: 'relative',
    left: theme.spacing(1),
    width: 0,
  },
  label: {
    color: theme.palette.grey[50],
  },
}))(FilterItem);
