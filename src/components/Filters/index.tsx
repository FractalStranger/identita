import React, { useCallback, useEffect } from 'react';
import classnames from 'classnames';
import lodash from 'lodash';

import { withStyles, WithStyles, Box, Typography, Button, Divider, Hidden } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

import FilterTypeShape from '../../shapes/FilterTypeShape';
import RegionShape from '../../shapes/RegionShape';
import ActiveFiltersShape from '../../shapes/ActiveFiltersShape';
import PointShape from '../../shapes/PointShape';

import FilterItem from './FilterItem';

import filterItems from '../../constants/filterItems';

type Props = {
  history: any;
  region: string;
  regions: RegionShape[];
  activeFilters: ActiveFiltersShape;
  activePoints: PointShape[];
  allPoints: PointShape[];
  mobileFiltersOpen: boolean;
  toggleMobileFiltersOpen: (bool: boolean) => void;
  regionDetailLoadedFirst: boolean;
  regionDetailError: any;
};

function Filters({
  classes,
  history,
  region,
  regions,
  activeFilters,
  activePoints,
  allPoints,
  mobileFiltersOpen,
  toggleMobileFiltersOpen,
  regionDetailLoadedFirst,
  regionDetailError,
}: Props & WithStyles) {
  const handleResetFilters = useCallback(() => {
    history.push(`/mapa/${region}`);
    toggleMobileFiltersOpen(false);
  }, [history, region, toggleMobileFiltersOpen]);

  useEffect(() => {
    if (mobileFiltersOpen) {
      document.body.classList.add('mobileMenuOpen');
      return;
    }
    document.body.classList.remove('mobileMenuOpen');
  }, [mobileFiltersOpen]);

  return (
    <>
      <Box
        className={classnames(
          classes.wrapper,
          !mobileFiltersOpen && "mobileHidden"
        )}
      >
        <Box
          className={classnames(
            classes.innerWrapper,
            "background-dark",
            "mui-absolute",
            !mobileFiltersOpen && "mobileHidden"
          )}
        >
          <Box className={classnames(classes.container, "container")}>
            <Box className={classes.inner}>
              <Hidden mdDown implementation="css">
                <Typography className={classes.frontLabel}>
                  Filtrovať podľa:
                </Typography>
              </Hidden>
              {filterItems.map((filterItem: FilterTypeShape) => (
                <FilterItem
                  key={filterItem.label}
                  data={filterItem}
                  region={region}
                  history={history}
                  regions={regions || null}
                  activeFilters={activeFilters}
                  activePoints={activePoints}
                  allPoints={allPoints}
                  toggleMobileFiltersOpen={toggleMobileFiltersOpen}
                  regionDetailLoadedFirst={regionDetailLoadedFirst}
                  regionDetailError={regionDetailError}
                />
              ))}
              {region !== "slovensko" && !lodash.isEmpty(activeFilters) && (
                <Button
                  onClick={handleResetFilters}
                  variant="text"
                  className={classes.reset}
                >
                  <ClearIcon /> Reset
                </Button>
              )}
            </Box>
          </Box>
          <Divider className={classes.divider} />
        </Box>
        <Box className={classes.filterMobileBar}>
          <Typography
            variant="h5"
            color="textSecondary"
            className={classes.mobileFrontLabel}
          >
            Filter
          </Typography>
          {!lodash.isEmpty(activeFilters) && (
            <Button
              onClick={handleResetFilters}
              variant="text"
              className={classes.resetMobile}
            >
              <Typography variant="subtitle1" component="span">
                Resetovať
              </Typography>
            </Button>
          )}
          <Hidden lgUp implementation="css">
            <Button
              className={classes.hamburgerButton}
              onClick={() => toggleMobileFiltersOpen(!mobileFiltersOpen)}
            >
              {mobileFiltersOpen && <ClearIcon />}
            </Button>
          </Hidden>
        </Box>
      </Box>
    </>
  );
}

export default withStyles((theme) => ({
  wrapper: {
    height: 72,
    position: "absolute",
    width: "100%",
    left: 0,
    top: 0,
    zIndex: 50,
    [theme.breakpoints.down("md")]: {
      height: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      "&.mobileHidden": {
        display: "none",
      },
    },
  },
  innerWrapper: {
    height: 72,
    position: "absolute",
    width: "100%",
    left: 0,
    top: 0,
    [theme.breakpoints.down("md")]: {
      height: "100%",
      width: "100%",
      "&.mobileHidden": {
        display: "none",
      },
    },
  },
  divider: {
    display: 'none',
    [theme.breakpoints.down("md")]: {
      display: "block",
      position: "absolute",
      left: 0,
      top: 56,
      width: "100%",
      ".mobileHidden &": {
        display: "none",
      },
    },
  },
  container: {
    width: "100%",
    [theme.breakpoints.up("xl")]: {
      width: 1220,
    },
    [theme.breakpoints.up("lg")]: {
      backgroundColor: theme.palette.grey[50],
      boxShadow: theme.shadows[3],
    },
    [theme.breakpoints.down("md")]: {
      padding: 0,
    },
  },
  inner: {
    display: "flex",
    alignItems: "center",
    height: "100%",
    position: "relative",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
      marginTop: theme.spacing(16),
    },
  },
  frontLabel: {
    fontWeight: "normal",
    fontSize: "18px",
    lineHeight: "22px",
    marginRight: theme.spacing(3.5),
    color: '#2c1c14',
  },
  filterMobileBar: {
    position: "absolute",
    left: 0,
    top: 0,
    width: "100%",
    height: "100%",
    display: 'none',
    [theme.breakpoints.down("md")]: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      height: "56px",
    },
  },
  mobileFrontLabel: {
    display: "none",
    zIndex: 5,
    [theme.breakpoints.down("md")]: {
      display: "inline-block",
      color: theme.palette.grey[50],
      ".mobileHidden &": {
        display: "none",
      },
    },
  },
  reset: {
    position: "absolute",
    right: 0,
    fontSize: '16px',
    '& svg': {
      marginRight: theme.spacing(1),
      marginTop: theme.spacing(0.5),
    },
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  resetMobile: {
    display: "none",
    position: "absolute",
    right: theme.spacing(2.5),
    [theme.breakpoints.down("md")]: {
      display: "inline-block",
      "& .MuiTypography-root": {
        fontWeight: "bold",
      },
      ".mobileHidden &": {
        display: "none",
      },
    },
  },
  hamburgerButton: {
    position: "absolute",
    top: "50%",
    transform: "translateY(-50%)",
    color: theme.palette.primary.light,
    left: 0,
    "& svg": {
      fontSize: "24px",
    },
  },
}))(Filters);
