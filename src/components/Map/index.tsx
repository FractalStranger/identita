import React, { useState, useEffect, useCallback } from 'react';
import lodash from 'lodash';
import classnames from 'classnames';

import { withStyles, WithStyles, Typography, Button, Hidden } from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';

import PointShape from '../../shapes/PointShape';
import RegionDetailShape from '../../shapes/RegionDetailShape';

const scriptUrl = 'https://api.mapy.cz/loader.js';

type Props = {
  regionParams: RegionDetailShape['params'];
  activePoints: PointShape[];
  mobileMapOpen: boolean;
  toggleMobileMapOpen: (bool: boolean) => void;
  region: string;
  regionDetailLoadedFirst: boolean;
  regionDetailError: any;
};

let layer: any;
let map: any;

function Map({
  classes,
  regionParams,
  activePoints,
  mobileMapOpen,
  toggleMobileMapOpen,
  region,
  regionDetailLoadedFirst,
  regionDetailError,
}: Props & WithStyles) {
  const [scriptLoaded, setScriptLoaded] = useState(0);
  const [mapCreated, setMapCreated] = useState(0);
  
  let samePositionPoisArray: { [key: string]: any } = {};

  const renderMarkerCard = useCallback((item: PointShape) => {
    return `<a href="${item.link}" target="_blank" rel="noopener noreferrer" class=${classes.anchor}>` +
              `<div>` + 
                `<div class="${classes.marginBottom2}">` +
                  `<p class="MuiTypography-root MuiTypography-body2 ${classes.poiCategory}">` +
                    `${!lodash.isEmpty(item.interest_area) && item.interest_area[0].label}` +
                  `</p>` +
                `</div>` +
                `<div class="${classes.marginBottom2}">` +
                  `<p class="MuiTypography-root MuiTypography-h5" title="${item.title.length > 50 ? item.title : ''}">` +
                    `${item.title.length > 50 ? item.title.substr(0, 50) + `...` : item.title}` +
                  `</p>` +
                `</div>` +
                `<div class="${classes.flexBox} ${item.location_name === null ? classes.hidden : ''}">` +
                  `<img src="${process.env.REACT_APP_PUBLIC_FOLDER}/pin.svg" alt="pin" />` +
                  `<p class="MuiTypography-root MuiTypography-body2">${item.location_name}</p>` +
                `</div>` +
              `</div>` +
            `</a>`;
  }, [classes]);

  const onScriptLoaded = useCallback(() => {
    window.Loader.async = true;
			window.Loader.load(null, null, () => {
				setScriptLoaded(1);
			});
  }, []);

  const placeItemIntoCircle = (items: number, order: number, long: number, lat: number) => {
    var numElements:number = items
      , angle = 0
      , radius = 0.0003
      , step = (2 * Math.PI) / numElements;
    angle = (order - 1) * step;
    var newLong = long + (radius * Math.cos(angle));
    var newLat = lat + (radius * Math.sin(angle));
    return [newLong, newLat];
  };

  const addMarker = useCallback((
    layer,
    item: PointShape,
    samePositionPoisArray: { [key: string]: any },
    order: number,
  ) => {
    var card = new window.SMap.Card();
    card.getContainer().classList.add(classes.markerCard);
    card.getHeader().innerHTML =
      `<a href="${item.link}" target="_blank" rel="noopener noreferrer" class=${classes.anchor}>` +
        `<img src='${item.image_url}' alt='Alt' />` +
      `</a>`;
    card.getBody().innerHTML = renderMarkerCard(item);

    // var options = {
    //   title: "Dobré ráno"
    // };
    let lng, lat;
    if (samePositionPoisArray) {
      var coords = Number(item.lng).toFixed(3) + ";" + Number(item.lat).toFixed(3);
      var items = samePositionPoisArray[coords];

      if (samePositionPoisArray[coords] > 1) {
        lng = Number(parseFloat(item.lng).toFixed(4));
        lat = Number(parseFloat(item.lat).toFixed(4));
        var newCoords = placeItemIntoCircle(items, order, lng, lat);
        lng = newCoords[0];
        lat = newCoords[1];
      } else {
        lng = item.lng;
        lat = item.lat;
      }
    }

    const center = window.SMap.Coords.fromWGS84(lng, lat);
    var marker = new window.SMap.Marker(center, null, item);
    marker.getContainer()[3].src = process.env.REACT_APP_PUBLIC_FOLDER + "/marker.svg";
    marker.decorate(window.SMap.Marker.Feature.Card, card);
    // coordinates.push(center);
    // markers.push(marker);
    layer.addMarker(marker);
  }, [classes, renderMarkerCard]);

  const addMarkers = useCallback((map, layer) => {
    let markersWithOrder:{ [key: string]: any } = {};
    let length: number = 1;
    activePoints.forEach((point: PointShape) => {
      if (!point.lat || !point.lng) return;
      var coords = Number(point.lng).toFixed(3) + ";" + Number(point.lat).toFixed(3);
      if (coords in samePositionPoisArray) {
        length++;
      } else {
        length = 1;
      }
      samePositionPoisArray[coords] = length;
      markersWithOrder = {
        ...markersWithOrder,
        [point.id]: length,
      };
    });

    activePoints.forEach((point: PointShape) => {
      if (!point.lat || !point.lng) return;
      const order = markersWithOrder[point.id];
      addMarker(layer, point, samePositionPoisArray, order);
    });
  }, [activePoints, samePositionPoisArray, addMarker]);

  useEffect(() => {
    if (mapCreated) return;
    if (scriptLoaded && !mapCreated) {
      const regParams = {
        lng:
          region === "slovensko" || !regionParams
            ? 19.5950799
            : Number(regionParams.lng),
        lat:
          region === "slovensko" || !regionParams
            ? 48.6832754
            : Number(regionParams.lat),
        zoom:
          region === "slovensko" || !regionParams
            ? 8
            : Number(regionParams.zoom),
      };
      var center = window.SMap.Coords.fromWGS84(regParams.lng, regParams.lat);
      map = new window.SMap(window.JAK.gel("map"), center, regParams.zoom);
      map.addDefaultLayer(window.SMap.DEF_BASE).enable();
      map.addDefaultControls();

      var sync = new window.SMap.Control.Sync();
      map.addControl(sync);

      layer = new window.SMap.Layer.Marker();
      map.addLayer(layer);
      layer.enable();
      var clusterer = new window.SMap.Marker.Clusterer(map);
      layer.setClusterer(clusterer);

      setMapCreated(1);

      return;
    }
    if (!mapCreated) {
      const script = document.createElement("script");
      script.src = scriptUrl;
      script.async = true;
      script.onload = () => onScriptLoaded();
      document.head.appendChild(script);
    }
  }, [scriptLoaded, mapCreated]);

  useEffect(() => {
    if (layer) {
      if (map._card) map._card._closeClick();
      layer.removeAll();
      if (activePoints && activePoints.length) {
        addMarkers(map, layer);
      }
    }
  }, [activePoints, mapCreated]);

  useEffect(() => {
    if (scriptLoaded && mapCreated) {
      const regParams = {
        lng: region === "slovensko" ? 19.5950799 : Number(regionParams.lng),
        lat: region === "slovensko" ? 48.6832754 : Number(regionParams.lat),
        zoom: region === "slovensko" ? 8 : Number(regionParams.zoom),
      };
      var center = window.SMap.Coords.fromWGS84(regParams.lng, regParams.lat);
      map.setCenterZoom(center, regParams.zoom);
    }
  }, [regionParams]);

  return (
    <div
      className={classnames(
        classes.wrapper,
        mobileMapOpen && classes.mobileMapOpen
      )}
    >
      <Hidden lgUp implementation="css">
        <div className={classes.regionInfo}>
          <Button
            className={classes.closeButton}
            variant="text"
            onClick={() => toggleMobileMapOpen(false)}
          >
            <ClearIcon />
          </Button>
          {((regionDetailLoadedFirst && !regionDetailError) || region === 'slovensko') && (
            <Typography variant="h5" className={classes.regionName}>
              {region === "slovensko" ? " Slovensko" : regionParams.title}
            </Typography>
          )}
        </div>
      </Hidden>
      {scriptLoaded ? (
        <div id="map" className={classes.map}></div>
      ) : (
        <div>map loading...</div>
      )}
    </div>
  );
}

export default withStyles((theme) => ({
  wrapper: {
    position: "absolute",
    bottom: 0,
    right: 0,
    // height: "calc(100% - 72px)",
    height: '100%',
    width: "calc(100% - 620px)",
    backgroundColor: theme.palette.primary.main,
    zIndex: 5,
    [theme.breakpoints.up("xl")]: {
      // width: "calc(((100vw - 1440px) / 2) + 1440px - 670px)",
      // width: 'calc(1220px - 620px)',
      // marginRight: 'calc((100vw - 1220px) / 2)',
      top: 0,
    },
    [theme.breakpoints.down("md")]: {
      width: "100%",
      // height: "calc(100% - 112px)",
      bottom: "auto",
      top: 0,
      height: "calc(100% - 57px)",
    },
  },
  mobileMapOpen: {
    [theme.breakpoints.down("md")]: {
      zIndex: 20,
    },
  },
  map: {
    height: "100%",
    width: "100%",
  },
  markerCard: {
    ".smap &.card": {
      display: "flex",
      height: 140,
      width: "456px !important",
      backgroundImage: `url(${process.env.REACT_APP_PUBLIC_FOLDER}/card-background.svg)`,
      backgroundSize: "460px auto",
      boxShadow: "none",
      backgroundColor: "transparent",
      padding: "8px",
      paddingBottom: "12px",
      "& img": {
        width: "auto",
        height: "auto",
        maxHeight: "100%",
        display: "block",
      },
      [theme.breakpoints.down("md")]: {
        backgroundImage: `url(${process.env.REACT_APP_PUBLIC_FOLDER}/card-background-mobile.svg)`,
        width: "300px !important",
        height: "138px",
        backgroundSize: "304px auto",
      },
    },
    "& .card-header": {
      width: 160,
      height: 120,
      overflow: "hidden",
      flex: "none",
      borderBottomLeftRadius: "3px",
      borderTopLeftRadius: "3px",
      [theme.breakpoints.down("md")]: {
        display: "none",
      },
    },
    ".smap &.card .card-body": {
      backgroundColor: "transparent",
      padding: 15,
      overflow: "visible",
    },
    ".smap &.card .close": {
      // display: 'none',
      top: theme.spacing(3),
      right: theme.spacing(3),
      "&:after": {
        color: theme.palette.text.primary,
      },
    },
  },
  flexBox: {
    display: "flex",
    alignItems: "center",
    "& .MuiTypography-root": {
      color: theme.palette.text.primary,
      marginLeft: theme.spacing(1.5),
    },
  },
  hidden: {
    display: "none",
  },
  marginBottom2: {
    marginBottom: theme.spacing(1.5),
  },
  poiCategory: {
    opacity: 0.64,
    color: theme.palette.text.primary,
    fontWeight: "bold",
  },
  anchor: {
    textDecoration: "none",
  },
  regionInfo: {
    backgroundColor: theme.palette.grey[50],
    height: 56,
    position: "relative",
    padding: theme.spacing(2.5),
    display: "flex",
    alignItems: "center",
  },
  regionName: {
    width: "80%",
    display: "inline-block",
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
  },
  closeButton: {
    position: "absolute",
    right: 0,
    top: "50%",
    transform: "translateY(-50%)",
  },
}))(Map);
