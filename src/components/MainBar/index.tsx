import React, { useState, useCallback } from 'react';
import classnames from 'classnames';
import Truncate from 'react-truncate';

import { withStyles, WithStyles, Box, Typography, Card, CircularProgress } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import RegionDetailShape from '../../shapes/RegionDetailShape';
import PointShape from "../../shapes/PointShape";

import Map from '../../containers/MapContainer';
import EntryList from '../../containers/EntryListContainer';

type Props = {
  regionDetail: RegionDetailShape;
  regionDetailLoaded: boolean;
  mobileMapOpen: boolean;
  region: string;
  regionDetailLoadedFirst: boolean;
  activePoints: PointShape[];
};

function MainBar({
  classes,
  regionDetail,
  regionDetailLoaded,
  mobileMapOpen,
  region,
  regionDetailLoadedFirst,
  activePoints,
}: Props & WithStyles) {
  const [infoExpanded, setInfoExpanded] = useState(false);

  const toggleExpandedInfo = useCallback(() => {
    setInfoExpanded(!infoExpanded);
  }, [infoExpanded]);

  return (
    <>
      {region !== "slovensko" && !regionDetailLoaded && (
        <Box
          height="100%"
          width="100%"
          display="flex"
          alignItems="center"
          justifyContent="center"
          position="absolute"
          left="0"
          top="0"
          zIndex="10"
          bgcolor="rgba(0, 0, 0, 0.5)"
          paddingTop="72px"
        >
          <CircularProgress />
        </Box>
      )}
      <Box
        className={classnames(
          classes.container,
          "container",
          mobileMapOpen && classes.lockedContainer
        )}
      >
        <Map region={region} />
        <div
          className={classnames(
            classes.wrapper,
            mobileMapOpen && classes.lockedWrapper
          )}
        >
          {(region === 'slovensko' || regionDetailLoadedFirst) && (
            <>
              <Card className={classes.mainCard}>
                <Box
                  mb={
                    regionDetail.params && regionDetail.params.description ? 6 : 2
                  }
                >
                  <Typography variant="h1" component="h1">
                    {region !== "slovensko"
                      ? regionDetail.params.title
                      : "Interaktívna databáza"}
                  </Typography>
                </Box>
                {region === "slovensko" ? (
                  <Box mt={6}>
                    <Typography>
                      Unikátna databáza materiálnej kultúry regiónov Slovenska.
                      Vychádza z etnografických výskumov a histórie, ale v prom rade
                      sa snaží zachytiť vnímanie a prezentovanie lokálnej identity v
                      jednotlivých regiónoch.
                      <br />
                      <br />
                      Chceme ňou poukázať na diverzitu a jedinečnosť jednotlivých
                      regiónov a lokalít, popísať a „ilustrovať“ regionálnu identitu
                      v materiálnej kultúre na Slovensku. Budeme radi, ak v nej
                      nájdete inšpiráciu pre svoje osobné či pracovné projekty,
                      výlety a ak prispejete ku rozvoju nových foriem ohľaduplného
                      turizmu a tým aj ku rozvoju a kultivovaniu samotného regiónu.
                      <br />
                      <br />
                      Pre zobrazenie regionálnych záznamov použite filter v
                      navigácii.
                    </Typography>
                  </Box>
                ) : (
                  <>
                    {regionDetail.params.description && (
                      <>
                        <Box mb={2}>
                          <Typography variant="h4" component="h4">
                            Charakteristika regiónu
                          </Typography>
                        </Box>
                        <Typography>
                          <Truncate
                            lines={!infoExpanded && 3}
                            ellipsis={
                              <span>
                                ...{" "}
                                <Typography
                                  variant="caption"
                                  className={classes.readMore}
                                  onClick={toggleExpandedInfo}
                                >
                                  Čítať celý popis <ChevronRightIcon />
                                </Typography>
                              </span>
                            }
                          >
                            <div
                              dangerouslySetInnerHTML={{ __html: regionDetail.params.description }}
                            />
                          </Truncate>
                          {infoExpanded && (
                            <Box mt={7}>
                              {" "}
                              <Typography
                                variant="caption"
                                className={classes.readMore}
                                onClick={toggleExpandedInfo}
                              >
                                <ChevronLeftIcon /> Skryť celý popis
                              </Typography>
                            </Box>
                          )}
                        </Typography>
                      </>
                    )}
                  </>
                )}
              </Card>
              {activePoints && regionDetailLoaded && <EntryList />}
            </>
          )}
        </div>
      </Box>
    </>
  );
}

export default withStyles((theme) => ({
  container: {
    width: "100%",
    position: 'relative',
    // overflowY: "auto",
    height: "calc(100vh - 72px)",
    backgroundColor: theme.palette.grey[50],
    [theme.breakpoints.up("xl")]: {
      width: 1220,
    },
    [theme.breakpoints.down("md")]: {
      height: "auto",
      padding: 0,
      display: "flex",
    },
  },
  lockedContainer: {
    [theme.breakpoints.down("md")]: {
      touchAction: "none",
      overflow: 'hidden',
      height: 'calc(100vh - 127px)',
    },
    '@media (max-width: 599px)': {
      height: 'calc(100vh - 169px)',
    }
  },
  lockedWrapper: {
    [theme.breakpoints.down("md")]: {
      visibility: "hidden",
    },
  },
  wrapper: {
    width: 700,
    position: "relative",
    // height: "calc(100vh - 144px)",
    overflowY: "auto",
    height: "100%",
    [theme.breakpoints.down("md")]: {
      height: "auto",
      width: "100%",
      padding: `0 ${theme.spacing(4)}px`,
      backgroundColor: theme.palette.grey[50],
      marginBottom: 56,
      zIndex: 10,
    },
  },
  breadcrumbs: {
    padding: `${theme.spacing(6.5)}px 0`,
  },
  breadcrumbLink: {
    color: theme.palette.text.primary,
    textDecoration: "none",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  mainCard: {
    padding: 0,
    marginBottom: theme.spacing(6),
    marginTop: theme.spacing(6),
    width: 570,
    [theme.breakpoints.down("md")]: {
      width: '100%',
    },
  },
  caption: {
    fontSize: 14,
    lineHeight: "18px",
    opacity: 0.64,
  },
  readMore: {
    cursor: "pointer",
    display: "flex",
    marginTop: theme.spacing(3),
    fontWeight: 600,
    "&:hover": {
      textDecoration: "underline",
    },
  },
}))(MainBar);
