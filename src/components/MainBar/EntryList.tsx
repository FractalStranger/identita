import React, { useState, useCallback, useEffect } from "react";
import ReactPaginate from "react-paginate";

import {
  withStyles,
  WithStyles,
  Box,
  Typography,
} from "@material-ui/core";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

import PointShape from "../../shapes/PointShape";

import EntryListItem from "./EntryListItem";

type Props = {
  activePoints: PointShape[];
};

function EntryList({
  classes,
  activePoints
}: Props & WithStyles) {
  const entriesPerPage = 16;
  const [page, setPage] = useState(1);

  const getActivePointsByPage = useCallback(
    page => {
      return activePoints.slice(
        (page - 1) * entriesPerPage,
        page * entriesPerPage
      );
    },
    [activePoints, entriesPerPage]
  );

  const [items, setItems] = useState(getActivePointsByPage(page));

  const handlePageClick = useCallback((data: any) => {
    setPage(data.selected + 1);
  }, []);

  useEffect(() => {
    if (activePoints.length) {
      setItems(getActivePointsByPage(page));
    }
  }, [page, activePoints.length]);

  if (!activePoints.length) return null;

  return (
    <Box className={classes.wrapper}>
      <Box mb={4}>
        <Typography variant="h2" component="h4">
          Záznamy
        </Typography>
      </Box>
      {items.map((entry: PointShape) => (
        <EntryListItem key={entry.id} item={entry} />
      ))}
      {activePoints.length / entriesPerPage > 1 && (
        <ReactPaginate
          previousLabel={<ChevronLeftIcon className={classes.chevron} />}
          nextLabel={<ChevronRightIcon className={classes.chevron} />}
          breakLabel={"..."}
          breakClassName={classes.breakLabel}
          pageCount={activePoints.length / entriesPerPage}
          marginPagesDisplayed={1}
          pageRangeDisplayed={3}
          disabledClassName={classes.disabled}
          onPageChange={handlePageClick}
          containerClassName={classes.pagination}
          pageClassName={classes.page}
          activeLinkClassName={classes.pageActive}
          pageLinkClassName={classes.pageLink}
          previousLinkClassName={classes.pageLink}
          nextLinkClassName={classes.pageLink}
        />
      )}
    </Box>
  );
}

export default withStyles(theme => ({
  wrapper: {
    width: 570,
    [theme.breakpoints.down("md")]: {
      width: '100%',
    },
  },
  pagination: {
    display: "flex",
    alignItems: "center",
    listStyleType: "none",
    color: '#454545',
    fontSize: 12,
    fontWeight: "bold",
    margin: `${theme.spacing(7)}px 0`,
    [theme.breakpoints.down('md')]: {
      margin: 0,
      padding: `${theme.spacing(6)}px 0`,
      display: 'flex',
      justifyContent: 'center',
    },
  },
  pageLink: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    cursor: "pointer",
    userSelect: "none",
    outline: "none",
    textAlign: "center",
    width: theme.spacing(5),
    height: theme.spacing(5),
    margin: `0 4px`,
    "&:hover": {
      textDecoration: "underline"
    }
  },
  disabled: {
    "& a": {
      cursor: "default",
      opacity: 0.5,
    }
  },
  pageActive: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.primary.main,
    borderRadius: "100%",
    cursor: "default",
    "&:hover": {
      textDecoration: "none"
    }
  },
  chevron: {
    // color: theme.palette.primary.light
  }
}))(EntryList);
