import React from 'react';
import { withRouter } from 'react-router-dom';
import lodash from 'lodash';

import {
  withStyles,
  WithStyles,
  Box,
  Typography,
  Card,
} from '@material-ui/core';

import PinIcon from '../Icons/Pin';

import PointShape from '../../shapes/PointShape';

type Props = {
  item: PointShape;
};

function EntryListItem({ classes, item }: Props & WithStyles) {
  return (
    <a href={item.link} target="_blank" rel="noopener noreferrer" className={classes.anchor}>
      <Card className={classes.card}>
        <Box display="flex" className={classes.flexWrapper}>
          <Box className={classes.imageWrapper}>
            <img src={item.image_url} alt={item.title} />
          </Box>
          <Box className={classes.textWrapper}>
            {!lodash.isEmpty(item.interest_area) && (
              <Box mb={1}>
                <Typography variant="body2" className={classes.category}>
                  {item.interest_area[0].label}
                </Typography>
              </Box>
            )}
            <Box mb={2}>
              <Typography variant="h4" component="h4" className={classes.title}>
                {item.title}
              </Typography>
            </Box>
            {item.location_name && (
              <Box display="flex" alignItems="center">
                <Box mr={1}>
                  <PinIcon height={20} />
                </Box>
                  <Typography variant="body2">{item.location_name}</Typography>
              </Box>
            )}
          </Box>
        </Box>
      </Card>
    </a>
  );
}

export default withStyles(theme => ({
  flexWrapper: {
    [theme.breakpoints.down("sm")]: {
      flexDirection: 'column',
    },
  },
  anchor: {
    textDecoration: 'none',
  },
  card: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(3),
    border: '1px solid #ece5dd',
  },
  imageWrapper: {
    width: 138,
    height: 104,
    overflow: 'hidden',
    marginRight: theme.spacing(4),
    flex: 'none',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down("sm")]: {
      height: '210px',
      width: '100%',
      marginBottom: theme.spacing(2.5),
    },
    '& img': {
      // width: 'auto',
      width: '100%',
      height: 'auto',
      // maxHeight: '100%',
      display: 'block',
    },
  },
  textWrapper: {
    padding: `${theme.spacing(1)}px 0`,
  },
  title: {
    fontSize: 20,
  },
}))(withRouter(EntryListItem));
