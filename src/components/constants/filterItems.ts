const filterItems = [
  {
    label: 'Lokalita',
    type: 'region',
  },
  {
    label: 'Oblasť záujmu',
    type: 'interest_area',
  },
  {
    label: 'Bod záujmu',
    type: 'interest_point',
  },
  {
    label: 'Remeslo',
    type: 'craft',
  },
  {
    label: 'Materiál',
    type: 'material',
  },
];

export default filterItems;