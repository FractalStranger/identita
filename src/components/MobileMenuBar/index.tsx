import React from 'react';
import { withRouter } from 'react-router-dom';

import { withStyles, WithStyles, Box, Typography } from '@material-ui/core';

import PinDarkIcon from '../Icons/PinDark';
import FiltersIcon from '../Icons/Filters';

type Props = {
  mobileFiltersOpen: boolean;
  toggleMobileFiltersOpen: (bool: boolean) => void;
  mobileMapOpen: boolean;
  toggleMobileMapOpen: (bool: boolean) => void;
};

function MobileMenuBar({
  classes,
  mobileFiltersOpen,
  toggleMobileFiltersOpen,
  mobileMapOpen,
  toggleMobileMapOpen,
}: Props & WithStyles) {
  return (
    <Box className={classes.wrapper}>
      <Box
        className={classes.toggler}
        onClick={() => toggleMobileFiltersOpen(!mobileFiltersOpen)}
      >
        <FiltersIcon height={18} />
        <Typography variant="caption">Filter</Typography>
      </Box>
      <Box
        className={classes.toggler}
        onClick={() => toggleMobileMapOpen(!mobileMapOpen)}
      >
        {mobileMapOpen ? (
          <>
            <Typography variant="caption">Záznamy</Typography>
          </>
        ) : (
          <>
            <PinDarkIcon height={18} />
            <Typography variant="caption">Mapa</Typography>
          </>
        )}
      </Box>
    </Box>
  );
}

export default withStyles(theme => ({
  wrapper: {
    position: 'fixed',
    zIndex: 45,
    display: 'flex',
    backgroundColor: theme.palette.grey[50],
    bottom: 0,
    left: 0,
    height: 56,
    width: '100%',
    boxShadow: theme.shadows[3],
  },
  toggler: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '50%',
    '&:first-child': {
      borderRight: `1px solid ${theme.palette.secondary.light}`,
    },
    '& .MuiTypography-root': {
      color: theme.palette.primary.dark,
    },
    '& svg': {
      marginRight: theme.spacing(1),
    },
  },
}))(withRouter(MobileMenuBar));
