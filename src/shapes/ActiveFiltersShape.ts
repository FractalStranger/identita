import FilterItemShape from "./FilterItemShape";

interface ActiveFiltersShape {
  interest_area: FilterItemShape[];
  interest_point: FilterItemShape[];
  craft: FilterItemShape[];
  material: FilterItemShape[];
  [index: string]: any;
}

export default ActiveFiltersShape;
