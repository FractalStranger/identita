interface FilterItemShape {
  label: string;
  id: string;
  [index: string]: string;
}

export default FilterItemShape;