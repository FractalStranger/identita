interface FilterTypeShape {
  label: string;
  type: string;
  [index: string]: string;
}

export default FilterTypeShape;
