interface EntryListItemShape {
  id: number;
  label: string;
  category: string;
  location: string;
  imageUrl: string;
  link: string;
}

export default EntryListItemShape;
