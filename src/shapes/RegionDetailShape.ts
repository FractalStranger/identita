import RegionShape from "./RegionShape";
import PointShape from "./PointShape";

interface RegionDetailShape {
  params: RegionShape;
  points: PointShape[];
}

export default RegionDetailShape;