import FilterItemShape from "./FilterItemShape";

interface PointShape {
  id: string;
  title: string;
  link: string;
  lat: string;
  lng: string;
  location_name: string;
  interest_area: FilterItemShape[];
  interest_point: FilterItemShape[];
  craft: FilterItemShape[];
  material: FilterItemShape[];
  image_url: string;
  [index: string]: any;
}

export default PointShape;