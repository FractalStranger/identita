interface RegionShape {
  id: string;
  title: string;
  description: string | null;
  zoom: string | null;
  lat: string;
  lng: string;
  slug: string;
}

export default RegionShape;