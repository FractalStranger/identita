const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = function override(config, env) {

  config.optimization.runtimeChunk = false;
  config.optimization.splitChunks = {
    cacheGroups: {
      default: false
    }
  };
  config.output.filename = 'static/js/[name].min.js',
  config.output.chunkFilename = 'static/js/[name].chunk.min.js',
  //   // key 5 = MiniCssExtractPlugin
  //   config.plugins[5].options.filename = 'static/css/[name].min.css';
  // config.plugins[5].options.chunkFilename = 'static/css/[name].chunk.min.css';
  config.plugins.push(new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: 'static/css/[name].min.css',
    chunkFilename: 'static/css/[name].chunk.css',
  }));

  return config;
};
